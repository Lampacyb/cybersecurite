# Obsidian and GitLab Pages Demo

This project demonstrates how to set up your Obsidian notes to be published using GitLab Pages and MkDocs.

See https://lampacyb.gitlab.io/cybersecurite for the rendered site.
