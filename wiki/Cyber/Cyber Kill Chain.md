![](https://tryhackme-images.s3.amazonaws.com/user-uploads/5fc2847e1bbebc03aa89fbf2/room-content/3e0e377e44467ea18ba06aa1f8f14665.png)

1 - Reconnaissance

	Obtenir des informations sur la victime et les tactiques utilisées pour l'attaque.

	Ex : Récolte des e-mails, OSINT et des réseaux sociaux, analyses du réseau

2 - Armement

	Les logiciels malveillants sont conçus en fonction des besoins et des intentions de l'attaque.

	Ex : Exploitation avec porte dérobée, document de bureau malveillant
	
3 - Livraison

	Couvre comment le logiciel malveillant serait livré au système de la victime.

	Ex : Courriel, liens Web, clé USB

4 - Exploitation

	Brisez les vulnérabilités du système de la victime pour exécuter du code et créer des tâches planifiées pour établir la persistance.

	Ex : EternalBlue, Zero-Logon, etc.
	
5 - Installation 

	Installez des logiciels malveillants et d'autres outils pour accéder au système de la victime.

	Ex : Transfert de mots de passe, portes dérobées, chevaux de Troie d'accès à distance

6 - Commandement et contrôle

	Contrôlez à distance le système compromis, diffusez des logiciels malveillants supplémentaires, déplacez-vous sur des actifs précieux et élevez les privilèges.


	Ex : Empire, Frappe de Cobalt, etc.

7 - Actions sur les objectifs

	Atteignez les objectifs visés par l'attaque : gain financier, espionnage d'entreprise et exfiltration de données.


	Ex : Cryptage des données, rançongiciel, dégradation publique
