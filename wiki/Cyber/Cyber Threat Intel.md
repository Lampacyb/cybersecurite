Connaitre son adversaire :
	Qui attaque ?
	Quelles sont leur motivations ?
	Quelles sont leur capacités ?
	Quel failles pourrais-je avoir ?

Il existe 4 types de classification de Threat Intel :

	Informations stratégiques
	Renseignements techniques
	Renseignements tactiques
	Renseignements opérationnels


# Cycle de vie 

## 1 - PLANNING ET DIRECTION

Identifier ce que l'on doit protéger et ce qui peut être en danger
Identifier les impacts / les pertes
Identifier comment protéger ces données
Identifier les ressources à utiliser pour protéger ces données

## 2 - COLLECTION

Rassembler les ressources de défense.

## 3 - TRAITEMENT

Utilisation des SIEM pour traiter, trier, organiser et extraire les données.

## 4 - ANALYSE

Enquêter sur une potentielle menace d'attaque
Définir un plan d'action pour éviter une attaque et défense l'infra
Renforcer les contrôles de sécurité et/ou justifier l'investissement pour plus de ressources

## 5 - DISSÉMINATION

Répartition des tâches entre les équipes

## 6 - RETOUR

Amélioration du processus selon les commentaires et les renseignements collectés.


# Normes et Cadres

## Cadre AT&CT :

Connaissances sur le comportement d'un adversaire axé sur les tactiques d'attaque.
https://attack.mitre.org/

## TAXII

Protocoles pour l'échange sécurisé d'informations afin d'avoir une réduction, une prévention et une détection des menaces en temps réel.
https://oasis-open.github.io/cti-documentation/taxii/intro

## STIX

Language pour la détection et l'information sur les menaces comme les éléments observables, les indicateurs, les campagnes d'attaques,...
https://oasis-open.github.io/cti-documentation/stix/intro

## Cyber Kill Chain 

Voir : [[Cyber Kill Chain]]

## Le modèle de diamant

Examine l'analyse des intrusions et le suivi des groupes d'attaques dans le temps. Il se concentre sur quatre domaines clés.

![](https://tryhackme-images.s3.amazonaws.com/user-uploads/5fc2847e1bbebc03aa89fbf2/room-content/e0d32b7a17c0b4326e596ae5fd9fb47e.png)
