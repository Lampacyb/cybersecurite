# Tools :

### Urlscan.io

Scanne un site web, résume les infos générales, fournit les informations sur les connexions HTTP, affiche les redirections, les liens sortant, les cookies et encore les adresses ip.
https://urlscan.io/

### Abuse.ch

Traque les malwares et les botnets : https://abuse.ch/
	1 - Malware Bazaar https://bazaar.abuse.ch/
		Base de donnée de malwares, upload et download.
	2 - FeodoTracker https://feodotracker.abuse.ch/
		Renseignements sur les serveurs botnet
	3 - SSL Blacklist https://sslbl.abuse.ch/
		Identifier et détecter les connexions SSL malveillantes
	4 - Urlhaus https://urlhaus.abuse.ch/
		Base de donnée d'url malveillantes
	5 - ThreatFox https://threatfox.abuse.ch/
		IOC - Indicateurs de compromission
	6 - YARAify https://yaraify.abuse.ch/
		Scan suspicious files

### Phishtool




